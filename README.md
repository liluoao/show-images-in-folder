## 展示一个文件夹中的所有图片

[![Email](https://img.shields.io/badge/%E9%82%AE%E7%AE%B1-liluoao%40qq.com-orange.svg?style=flat-square)](mailto:liluoao@qq.com)
[![SegmentFault](https://img.shields.io/badge/SegmentFault-李罗奥-brightgreen.svg?style=flat-square)](https://segmentfault.com/u/liluoao)
[![Juejin](https://img.shields.io/badge/掘金-李罗奥-blue.svg?style=flat-square)](https://juejin.im/user/5a19374cf265da4332274600)

[Demo](http://ngu.in/show-all-images-in-a-folder-with-php/)

### 设置

```php
# 图片文件夹路径
$imageFolder = 'img/';

# 需要展示的类型
$imageTypes = '{*.jpg,*.JPG,*.jpeg,*.JPEG,*.png,*.PNG,*.gif,*.GIF}';

# 是否按名称排序
# 否则按时间排序
$sortByImageName = false;
```
